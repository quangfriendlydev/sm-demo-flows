import 'reflect-metadata';
//https://github.com/onflow/kitty-items/blob/01aa1a24856687383b7b43a841973f3c9285d996/api/src/workers/base-event-handler.ts
// BaseEventHandler will iterate through a range of block_heights and then run a callback to process any events we
// are interested in. It also keeps a cursor in the database so we can resume from where we left off at any time.
// get lastest block -> scan all event in block -> save cursor -> send event callback. 
import * as fcl from "@onflow/fcl"
import { plainToInstance, Type } from "class-transformer"
class FlowEvent<T>{

    blockId: string

    blockHeight: number

    @Type(() => Date)
    blockTimestamp: Date

    type: string

    transactionId: string

    transactionIndex: number

    eventIndex: number

    data: T
}
function isObjectEmpty(obj) {
    for (const _ in obj) {
        return false
    }
    return true
}
type EventCallBack<T = any> = (data: FlowEvent<T>) => void
class EventsHandler {

    // {eventname:{
    //   [EventHandlerCallback()]
    // , currentBlockHeight:Number
    // }
    private static eventHandles: {
        [eventName: string]: {
            callbacks: Set<EventCallBack>,
            currentBlockHeight: number,
            currentTimeoutID?: number
        }
    } = {}
    private static stepSize = 200;
    private static stepTimeMs = 1000;
    private static isRun = false
    private constructor() { }
    static subscribe<T>(eventName: string, cb: EventCallBack<T>) {
        if (eventName && cb) {
            if (!(eventName in EventsHandler.eventHandles)) {
                EventsHandler.eventHandles[eventName] = {
                    callbacks: new Set(),
                    currentBlockHeight: -1
                }
            }
            EventsHandler.eventHandles[eventName].callbacks.add(cb);
        } else {
            throw new Error("event name or call back is not defined");
        }
        if (!EventsHandler.isRun) {
            EventsHandler.isRun = true
            EventsHandler.run()
        }
    }
    static unsubscribe(eventName: string, cb: EventCallBack) {
        const obj = EventsHandler.eventHandles[eventName]
        //remove callback
        obj.callbacks.delete(cb)
        if (obj.callbacks.size == 0) {
            obj ? clearTimeout(obj.currentTimeoutID) : 1
            delete EventsHandler.eventHandles[eventName]
            if (isObjectEmpty(EventsHandler.eventHandles)) {
                this.isRun = false
            }
        }
    }
    static async getLatestBlockHeight(): Promise<number> {
        const latestSealedBlock = await fcl.send([
            fcl.getBlock(true), // isSealed = true
        ])
            .then(fcl.decode);
        return latestSealedBlock.height as number;
    }
    static async getBlockRange(currentBlockHeight: number, startingBlockHeight: number) {
        let fromBlock =
            startingBlockHeight < currentBlockHeight
                ? currentBlockHeight
                : startingBlockHeight;
        let toBlock = await EventsHandler.getLatestBlockHeight();
        if (toBlock - fromBlock > EventsHandler.stepSize) {
            fromBlock = toBlock - 1;
            toBlock = await EventsHandler.getLatestBlockHeight();
        }
        if (fromBlock > toBlock) {
            fromBlock = toBlock;
            toBlock = currentBlockHeight;
        }

        return { fromBlock, toBlock };
    }
    private static async emit<T>(eventNames: string, data: T) {
        let classData = plainToInstance<FlowEvent<T>, T>(FlowEvent, data)
        for (const cb of EventsHandler.eventHandles[eventNames].callbacks)
            cb(classData)
    }
    private static async run() {
        // console.log("fetching latest block height");

        let startingBlockHeight = await EventsHandler.getLatestBlockHeight();

        // TODO: remove this once SDK fix is released: https://github.com/onflow/flow-js-sdk/pull/714
        if (startingBlockHeight === 0) {
            startingBlockHeight = 1;
        }
        // console.log("latestBlockHeight =", startingBlockHeight);

        Object.entries(EventsHandler.eventHandles).forEach(async ([eventName, obj]) => {
            const poll = async () => {
                let { fromBlock, toBlock } = await EventsHandler.getBlockRange(
                    obj.currentBlockHeight > 0 ? obj.currentBlockHeight : startingBlockHeight,
                    startingBlockHeight
                ).catch(e => {
                    console.warn("Error retrieving block range:", e);
                    return { fromBlock: -1, toBlock: -1 };
                });
                if (toBlock >= 0 && fromBlock >= 0 && fromBlock < toBlock) {
                    try {
                        fromBlock += 1
                        const eventsResponse = await fcl.send([
                            fcl.getEventsAtBlockHeightRange(eventName, fromBlock, toBlock)
                        ]).then(fcl.decode);

                        if (eventsResponse.length) {
                            eventsResponse.forEach(async (event) => await EventsHandler.emit(eventName, event));
                            // Record the last block that we saw an event for
                            obj.currentBlockHeight = toBlock;
                        }
                    } catch (e) {
                        console.error(
                            `Error retrieving events for block range fromBlock=${fromBlock} toBlock=${toBlock}`,
                            e
                        );
                    }
                }
                obj.currentTimeoutID = await setTimeout(poll, this.stepTimeMs);
            }
            await poll();
        })
    }

}
function getEventName(address: string, contract: string, eventName: string) {
    return `A.${contract}.${address}.${eventName}`
}
type EventRegister = {
    subscribe: <T = any>(callback: EventCallBack<T>) => Omit<EventRegister, "subscribe">
    unsubscribe: (cb?: Function) => void
}
export function Events(eventIdentify: string): EventRegister
export function Events(address: string, contract: string, eventName: string): EventRegister
export function Events(eventIdentifyOraddress: string, contract?: string, eventName?: string): EventRegister {
    let eventIdentify = eventIdentifyOraddress
    if (contract && eventName) {
        eventIdentify = getEventName(eventIdentifyOraddress, contract, eventName)
    }
    let globalCallback: EventCallBack;
    return {
        subscribe: function <T>(callback: EventCallBack) {
            EventsHandler.subscribe<T>(eventIdentify, callback)
            globalCallback = callback
            return { unsubscribe: this.unsubscribe };
        },
        unsubscribe: (cb: Function) => {
            EventsHandler.unsubscribe(eventIdentify, globalCallback)
            cb && cb()
        }
    } as EventRegister
}