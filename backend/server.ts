import { config } from "@onflow/fcl"
import { Events } from "./event-handler"
import db from "quick.db"
config({
    "accessNode.api": "http://localhost:8080", // Mainnet: "https://access-mainnet-beta.onflow.org"
})
Events("A.f8d6e0586b0a20c7.Marketplace.TradeEvent").subscribe((event) => {
    db.push("TradeRecord", event.data)
    // console.log(event.data, Date.now());

})
Events("A.f8d6e0586b0a20c7.NFTConstract.MintEvent").subscribe((event) => {
    db.push("MintRecord", event.data)
    // console.log(event.data, Date.now());
})