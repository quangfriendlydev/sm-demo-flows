
import NFTConstract from "./NFT.cdc"
pub contract Marketplace {

    // Declare a public field of type String.
    //
    // All fields must be initialized in the init() function.
    pub let MarketPath: PublicPath 
    pub let nftStoragePath: StoragePath 
    pub let minterStoragePath: StoragePath
    pub let MinterPrivatePath: PrivatePath
   

    pub event TradeEvent(from:Address,to:Address,id:UInt64)
    
    pub resource NFTStore {
        access(all) let nfts:@{UInt64: NFTConstract.NFT}
        init() {
            self.nfts <- {}
        }
        destroy(){
            destroy self.nfts
        }
        pub fun idExists(_ id: UInt64): Bool {
            return self.nfts[id] != nil
        }
        pub fun getIDs(): [UInt64] {
            return self.nfts.keys
        }
        pub fun getNFTImages() :{UInt64:{String: AnyStruct}}{
            let values : {UInt64:{String: AnyStruct}}={}
            for key in self.nfts.keys {
                let value:&NFTConstract.NFT = &self.nfts[key] as &NFTConstract.NFT
                values[key]= value.getReport()
            }
            return values
        }
        pub fun getNFTImageByID(_ id:UInt64) :{String: AnyStruct}{
            let value:&NFTConstract.NFT = &self.nfts[id] as &NFTConstract.NFT
            return value.getReport()
        }
        pub fun takeNft(_ key:UInt64):@NFTConstract.NFT {
            let token <- self.nfts.remove(key: key)!
            return <-token
        }

        pub fun putNFT(_ nft: @NFTConstract.NFT){
            // add the new token to the dictionary with a force assignment
            // if there is already a value at that key, it will fail and revert
            self.nfts[nft.id] <-! nft
        }
    }
    // The init() function is required if the contract contains any fields.
    init() {
        self.nftStoragePath=/storage/marketplace
        self.minterStoragePath=/storage/minter
        self.MarketPath=/public/marketplace
        self.MinterPrivatePath=/private/minter
        
        self.account.save<@NFTConstract.NFTMinter>(<- NFTConstract.createMinter(), to: self.minterStoragePath)
        self.account.save<@NFTStore>(<- self.createStorage(), to: self.nftStoragePath)
        self.account.link<&NFTStore>(self.MarketPath,target:self.nftStoragePath)
        self.account.link<&NFTConstract.NFTMinter>(self.MinterPrivatePath, target:self.minterStoragePath)
    }
    pub fun createStorage(): @NFTStore{
            return <- create NFTStore() 
    }
    pub fun emitTradeEvent(from:Address,to:Address,id:UInt64){
        emit TradeEvent(from:from,to:to,id:id)
    }
}
