// HelloWorld.cdc
//
// Welcome to Cadence! This is one of the simplest programs you can deploy on Flow.
//
// The HelloWorld contract contains a single string field and a public getter function.
//
// Follow the "Hello, World!" tutorial to learn more: https://docs.onflow.org/cadence/tutorial/02-hello-world/
access(all) contract NFTConstract {

    // Declare a public field of type String.
    //
    // All fields must be initialized in the init() function.
    pub resource NFT {
        pub let id: UInt64
        pub var metadata: {String: String}?
        access(contract) init(_ id:UInt64, _ metadata:{String: String}? ){
            self.id=id
            self.metadata=metadata
        }
        pub fun getReport(): {String: AnyStruct} {
            let mixedValues:{String: AnyStruct}= {}
            mixedValues["id"]=self.id
            mixedValues["metadata"]=self.metadata
            return mixedValues
        }
    }

    pub event MintEvent(id:UInt64,metadata:{String: String}?)
    
    pub resource NFTMinter {
        pub var currentID: UInt64
        init( ){
            self.currentID=0
        }
        pub fun createNFT(_ metadata:{String: String}? ): @NFT{
            let nft:@NFT <- create NFT(self.currentID, metadata) 
            self.currentID = self.currentID+1 
            emit MintEvent(id: nft.id ,metadata: nft.metadata)
            return <- nft
        }
    }
    pub fun createMinter(): @NFTMinter{
            return <- create NFTMinter()
    }
}
