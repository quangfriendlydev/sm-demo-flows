import Marketplace from 0x01
import NFTConstract from 0x02
transaction(imageUrl:String) {
  let minterRef: &NFTConstract.NFTMinter
  let nftStoreRef: &Marketplace.NFTStore
  prepare(acct: AuthAccount) {
    self.minterRef = acct.getCapability<&NFTConstract.NFTMinter>(Marketplace.MinterPrivatePath).borrow()
            ?? panic("No permission in Store front")
    self.nftStoreRef = acct.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow()
          ?? panic("No permission when access nft store of marketplace")
 
  }
  execute {
    let newNFT:@NFTConstract.NFT<- self.minterRef.createNFT({
       "imageUrl": imageUrl
    })
    self.nftStoreRef.putNFT(<-newNFT)
    log(self.nftStoreRef.getIDs())
  }
}