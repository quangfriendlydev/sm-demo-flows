import Marketplace from 0x01
import NFTConstract from 0x02
pub fun main(address:Address,id:UInt64):{String: AnyStruct}{
    let marketplaceRef = getAccount(address).getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow() ?? panic("no store")
       return marketplaceRef.getNFTImageByID(id)
}
