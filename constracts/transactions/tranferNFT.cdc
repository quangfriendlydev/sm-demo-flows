import Marketplace from 0xMARKETPLACE_ADDRESS
import NFTConstract from 0xNFTCONTRACT_ADDRESS
transaction (id:UInt64){
    let marketplaceRef: &Marketplace.NFTStore
    let authNFtStoreRef: &Marketplace.NFTStore
    let fromAddress:Address
    let toAddress:Address
    prepare(acct: AuthAccount) {
        let fromAccount:PublicAccount =getAccount(0xMARKETPLACE_ADDRESS)
        self.marketplaceRef = fromAccount.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow()
                ?? panic("No permission when minter")
        var cap : Capability<&Marketplace.NFTStore>  = acct.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath) 
        if (!cap.check()){
            acct.save<@Marketplace.NFTStore>(<-Marketplace.createStorage(),to:/storage/NFTs)
            acct.link<&Marketplace.NFTStore>(Marketplace.MarketPath,target:/storage/NFTs)
        }
        self.authNFtStoreRef= cap.borrow() ?? panic("something went wrong")
        self.fromAddress= fromAccount.address
        self.toAddress= acct.address
    }
    execute {
        // in the future we has to implement pay for token
        // take  Funginable from AuthAccount   
        // pass to takeNft method. takeNft will take response for verify money enough to get Nft.
        let nft <-self.marketplaceRef.takeNft(id)
        let nftid = nft.id
        self.authNFtStoreRef.putNFT(<-nft)
        Marketplace.emitTradeEvent(from:self.fromAddress,to:self.toAddress,id:nftid)
    }
}