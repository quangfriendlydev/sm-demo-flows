import * as fcl from "@onflow/fcl";
export async function getListNFT(address, id) {
    var data = await fcl.send([
        fcl.script`
            import Marketplace from 0xMARKETPLACE_ADDRESS
            import NFTConstract from 0xNFTCONTRACT_ADDRESS
            pub fun main(address:Address):{UInt64: {String: AnyStruct}}{
                let marketplaceRef = getAccount(address).getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow() ?? panic("no store")
                return marketplaceRef.getNFTImages()
            }
          `,
        fcl.args([
            fcl.arg(address, fcl.t.Address), // a
        ]),
    ]).then(fcl.decode)

    return data;
}
export async function getNFTByID(address, id) {
    var data = await fcl.send([
        fcl.script`
        import Marketplace from 0xMARKETPLACE_ADDRESS
        import NFTConstract from 0xNFTCONTRACT_ADDRESS
        pub fun main(address:Address,id:UInt64):{String: AnyStruct}{
            let marketplaceRef = getAccount(address).getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow() ?? panic("no store")
               return marketplaceRef.getNFTImageByID(id)
        }
          `,
        fcl.args([
            fcl.arg(address, fcl.t.Address), // a
            fcl.arg(id, fcl.t.Int64), // b
        ]),
    ]).then(fcl.decode)

    return data
}
export async function mintNewNFT(imageUrl) {
    var response = await fcl.send([
        fcl.transaction`
        import Marketplace from 0xMARKETPLACE_ADDRESS
import NFTConstract from 0xNFTCONTRACT_ADDRESS
    transaction(imageUrl:String) {
        let minterRef: &NFTConstract.NFTMinter
        let nftStoreRef: &Marketplace.NFTStore
        prepare(acct: AuthAccount) {
            self.minterRef = acct.getCapability<&NFTConstract.NFTMinter>(Marketplace.MinterPrivatePath).borrow()
                    ?? panic("No permission in Store front")
            self.nftStoreRef = acct.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow()
                ?? panic("No permission when access nft store of marketplace")
        
        }
        execute {
            let newNFT:@NFTConstract.NFT<- self.minterRef.createNFT({
            "imageUrl": imageUrl
            })
            self.nftStoreRef.putNFT(<-newNFT)
            log(self.nftStoreRef.getIDs())
        }
    }
          `,
        fcl.limit(100),
        fcl.proposer(fcl.currentUser().authorization),
        fcl.authorizations([
            fcl.currentUser().authorization,
        ]),
        fcl.payer(fcl.currentUser().authorization),
        fcl.args([
            fcl.arg(imageUrl, fcl.t.String), // a
        ]),
    ])
    var transaction = await fcl.tx(response).onceSealed()
    console.log(transaction)
    return transaction
}
export async function tranferNFTToUser(id) {
    console.log(id);
    var response = await fcl.send([
        fcl.transaction`
        import Marketplace from 0xMARKETPLACE_ADDRESS
        import NFTConstract from 0xNFTCONTRACT_ADDRESS
        transaction (id:UInt64){
            let marketplaceRef: &Marketplace.NFTStore
            let authNFtStoreRef: &Marketplace.NFTStore
            let fromAddress:Address
            let toAddress:Address
            prepare(acct: AuthAccount) {
                let fromAccount:PublicAccount =getAccount(0xMARKETPLACE_ADDRESS)
                self.marketplaceRef = fromAccount.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath).borrow()
                        ?? panic("No permission when minter")
                var cap : Capability<&Marketplace.NFTStore>  = acct.getCapability<&Marketplace.NFTStore>(Marketplace.MarketPath) 
                if (!cap.check()){
                    acct.save<@Marketplace.NFTStore>(<-Marketplace.createStorage(),to:/storage/NFTs)
                    acct.link<&Marketplace.NFTStore>(Marketplace.MarketPath,target:/storage/NFTs)
                }
                self.authNFtStoreRef= cap.borrow() ?? panic("something went wrong")
                self.fromAddress= fromAccount.address
                self.toAddress= acct.address
            }
            execute {
                // in the future we has to implement pay for token
                // take  Funginable from AuthAccount   
                // pass to takeNft method. takeNft will take response for verify money enough to get Nft.
                let nft <-self.marketplaceRef.takeNft(id)
                let nftid = nft.id
                self.authNFtStoreRef.putNFT(<-nft)
                Marketplace.emitTradeEvent(from:self.fromAddress,to:self.toAddress,id:nftid)
            }
        }
          `,
        fcl.limit(100),
        fcl.proposer(fcl.currentUser().authorization),
        fcl.authorizations([
            fcl.currentUser().authorization,
        ]),
        fcl.payer(fcl.currentUser().authorization),
        fcl.args([
            fcl.arg(Number.parseInt(id), fcl.t.UInt64), // a
        ]),
    ])
    var transaction = await fcl.tx(response).onceSealed()
    console.log(transaction)
    return transaction
}
