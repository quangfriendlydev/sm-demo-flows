import { config } from "@onflow/fcl";

config({
    "accessNode.api": "http://localhost:8080", // Mainnet: "https://access-mainnet-beta.onflow.org"
    "discovery.wallet": "http://localhost:3000/fcl/authn", // Mainnet: "https://fcl-discovery.onflow.org/authn"
    "env": "local",
    "fcl.appDomainTag": "harness-app",
    "app.detail.title": "Onflow",
}).put("0xMARKETPLACE_ADDRESS", "0xf8d6e0586b0a20c7")
    .put("0xNFTCONTRACT_ADDRESS", "0xf8d6e0586b0a20c7")