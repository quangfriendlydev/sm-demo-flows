import Head from 'next/head'
import "../config";
import * as fcl from "@onflow/fcl";
import { useEffect, useState } from "react"
import { getListNFT, mintNewNFT, tranferNFTToUser } from "../api/script"
export default function Home() {
  const [user, setUser] = useState({ loggedIn: null })
  const [userAccount, setUserBalance] = useState({})
  const [marketplaceItem, setMarketplace] = useState({})
  const [myListItem, setMyListItem] = useState({})
  const [inputString, setInputString] = useState("")
  useEffect(() => fcl.currentUser.subscribe(setUser), [])
  useEffect(() => {
    if (user && user.addr)
      fcl.send([fcl.getAccount(user.addr)])
        .then(fcl.decode)
        .then(balance => {
          console.log(balance);
          setUserBalance(balance)
        })
  }, [user])
  useEffect(() => {
    console.log(userAccount);
    console.log(user, '- Has changed')
  }, [user, userAccount]) // <-- here put the parameter to listen
  useEffect(() => {
    fcl.config().get("0xMARKETPLACE_ADDRESS", "0x01").then(async addr => {
      const list = await getListNFT(addr)
      setMarketplace(list)
    });
  }, [])
  useEffect(() => {
    if (user && user.addr) {
      console.log("user address", user.addr)
      getListNFT(user.addr).then(list => {
        setMyListItem(list)
      })
    } else {
      setMyListItem({})
    }
  }, [user])
  const AuthedState = () => {
    return (
      <div>
        <div>Address: {user?.addr ?? "No Address"}</div>
        <button onClick={fcl.unauthenticate}>Log Out</button>
      </div>
    )
  }

  const UnauthenticatedState = () => {
    return (
      <div>
        <button onClick={fcl.logIn}>Log In</button>
        <button onClick={fcl.signUp}>Sign Up</button>
      </div>
    )
  }

  const BalanceState = () => {
    return (
      <div>
        <a> Balance:{userAccount.balance} Flow</a>
      </div>
    )
  }

  return (
    <div>
      <Head>
        <title>FCL Quickstart with NextJS</title>
        <meta name="description" content="My first web3 app on Flow!" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <h1>Flow App</h1>
      {user.loggedIn
        ? <> <AuthedState /> <BalanceState /></>
        : <UnauthenticatedState />
      }
      <input value={inputString} onInput={e => setInputString(e.target.value)} />
      <button onClick={() => mintNewNFT(inputString)} >Add NFT</button>
      <h1>Marketplace</h1>
      <div id="marketplace">
        {
          Object.keys(marketplaceItem).map((id) => (
            <div key={id} >
              <img src={marketplaceItem[id].metadata.imageUrl} height={100} width={100} />
              <p> {id}</p>
              <p>{JSON.stringify(marketplaceItem[id].metadata)}</p>
              <button onClick={() => tranferNFTToUser(id)}>Buy</button>
            </div>
          ))}
      </div>
      <h1>My List Item</h1>
      <div id="traded">

        {Object.keys(myListItem).map((id) => (
          <div key={id} >
            <img src={myListItem[id].metadata.imageUrl} height={100} width={100} />
            <p> {id}</p>
            <p>{JSON.stringify(myListItem[id].metadata)}</p>
          </div>
        ))}
      </div>
    </div>
  );
}
